package com.obvious.nasapp.di.component

import android.content.Context
import com.obvious.nasapp.NASAApplication
import com.obvious.nasapp.di.module.ApplicationTestModule
import com.obvious.nasapp.di.component.DaggerTestComponent
import org.junit.rules.TestRule
import org.junit.runner.Description
import org.junit.runners.model.Statement

class TestComponentRule(private val context: Context) : TestRule {

    private var testComponent: TestComponent? = null

    fun getContext() = context

   // fun getDatabaseService() = testComponent?.getDatabaseService()

    private fun setupDaggerTestComponentInApplication() {
        val application = context.applicationContext as NASAApplication
        testComponent = DaggerTestComponent.builder()
            .applicationTestModule(ApplicationTestModule(application))
            .build()
        application.setComponent(testComponent!!)
    }

    override fun apply(base: Statement, description: Description?): Statement {
        return object : Statement() {
            @Throws(Throwable::class)
            override fun evaluate() {
                try {
                    setupDaggerTestComponentInApplication()
                    base.evaluate()
                } finally {
                    testComponent = null
                }
            }
        }
    }

}