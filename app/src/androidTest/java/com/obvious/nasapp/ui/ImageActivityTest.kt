package com.obvious.nasapp.ui

import android.content.Intent
import androidx.lifecycle.Lifecycle
import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.Espresso
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.intent.Intents
import androidx.test.espresso.intent.matcher.IntentMatchers.hasComponent
import androidx.test.espresso.intent.rule.IntentsTestRule
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.platform.app.InstrumentationRegistry
import com.obvious.nasapp.R
import com.obvious.nasapp.di.component.TestComponentRule
import com.obvious.nasapp.ui.imageDetails.ImageDetailsActivity
import com.obvious.nasapp.ui.imageUi.ImageActivity
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.RuleChain
import androidx.test.InstrumentationRegistry.getTargetContext

import android.content.ComponentName
import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.intent.Intents.*

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers.*
import com.obvious.nasapp.ui.imageUi.adapter.NasaAdapter
import com.obvious.nasapp.utils.common.Constants


class ImageActivityTest {

    private val component =
        TestComponentRule(InstrumentationRegistry.getInstrumentation().targetContext)

    private val main = IntentsTestRule(ImageActivity::class.java, false, false)

    @get:Rule
    val chain = RuleChain.outerRule(component).around(main)

    @Before
    fun setup() {

    }

    @Test
    fun imageActivity_starts_on_launch() {
        main.launchActivity(Intent(component.getContext(), ImageActivity::class.java))
        Espresso.onView(withId(R.id.rv_images))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))

    }
    @Test
    fun imageActivity_recyclerview_on_item_click(){
        main.launchActivity(Intent(component.getContext(), ImageActivity::class.java))
        onView(withId(R.id.rv_images))
            .perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(3,ViewActions.click()))

    }


    @Test
    fun imageActivity_launch_intent_image_details() {
        main.launchActivity(Intent(component.getContext(), ImageActivity::class.java))
        onView(withId(R.id.rv_images))
            .perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(3,ViewActions.click()))

        val intent = Intent()

        intent.putExtra(
            Constants.NASA_IMAGE,
            "https://apod.nasa.gov/apod/image/1912/M27_Mazlin_2000.jpg"
        )
        intent.putExtra(Constants.NASA_IMAGE_POSITION, "")
        intent.putExtra(Constants.NASA_IMAGE_TITLE, "M27: The Dumbbell Nebula")
        intent.putExtra(
            Constants.NASA_IMAGE_DESC,
            "s this what will become of our Sun? Quite possibly.  The first hint of our Sun's future was discovered inadvertently in 1764. At that time, Charles Messier was compiling a list of diffuse objects not to be confused with comets. The 27th object on Messier's list, now known as M27 or the Dumbbell Nebula, is a planetary nebula, the type of nebula our Sun will produce when nuclear fusion stops in its core. M27 is one of the brightest planetary nebulae on the sky, and can be seen toward the constellation of the Fox (Vulpecula) with binoculars. It takes light about 1000 years to reach us from M27, featured here in colors emitted by hydrogen and oxygen. Understanding the physics and significance of M27 was well beyond 18th century science. Even today, many things remain mysterious about bipolar planetary nebula like M27, including the physical mechanism that expels a low-mass star's gaseous outer-envelope, leaving an X-ray hot white dwarf.   APOD across world languages: Arabic, Catalan, Chinese (Beijing), Chinese (Taiwan), Croatian, Czech, Dutch, Farsi, French, French, German, Hebrew, Indonesian, Japanese, Korean, Montenegrin, Polish, Russian, Serbian, Slovenian,  Spanish and Ukrainian"
        )
        main.launchActivity(Intent(component.getContext(), ImageDetailsActivity::class.java))
    }
}