package com.obvious.nasapp.ui

import android.content.Intent
import androidx.lifecycle.Lifecycle
import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.Espresso
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.intent.Intents
import androidx.test.espresso.intent.matcher.IntentMatchers.hasComponent
import androidx.test.espresso.intent.rule.IntentsTestRule
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.platform.app.InstrumentationRegistry
import com.obvious.nasapp.R
import com.obvious.nasapp.di.component.TestComponentRule
import com.obvious.nasapp.ui.imageDetails.ImageDetailsActivity
import com.obvious.nasapp.ui.imageUi.ImageActivity
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.RuleChain
import androidx.test.InstrumentationRegistry.getTargetContext

import android.content.ComponentName
import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.intent.Intents.*

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers.*
import com.obvious.nasapp.ui.imageUi.adapter.NasaAdapter
import com.obvious.nasapp.utils.common.Constants
import org.hamcrest.CoreMatchers.not


class ImageDetailsActivityTest {

    private val component =
        TestComponentRule(InstrumentationRegistry.getInstrumentation().targetContext)

    private val main = IntentsTestRule(ImageDetailsActivity::class.java, false, false)

    @get:Rule
    val chain = RuleChain.outerRule(component).around(main)

    @Before
    fun setup() {

    }

   @Test
   fun checkTextViewTitle_isDisplayed_and_not_empty(){
       main.launchActivity(Intent(component.getContext(), ImageDetailsActivity::class.java))
       onView(withId(R.id.tv_title)).check(matches(not(withText(""))));
   }
}