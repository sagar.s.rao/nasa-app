package com.obvious.nasapp

import android.app.Application
import com.obvious.nasapp.di.component.ApplicationComponent
import com.obvious.nasapp.di.component.DaggerApplicationComponent
import com.obvious.nasapp.di.module.ApplicationModule


class NASAApplication : Application() {

    lateinit var applicationComponent: ApplicationComponent

    override fun onCreate() {
        super.onCreate()
        injectDependencies()
    }

    private fun injectDependencies() {
        applicationComponent = DaggerApplicationComponent
            .builder()
            .applicationModule(ApplicationModule(this))
            .build()
        applicationComponent.inject(this)
    }

    // Needed to replace the component with a test specific one
    fun setComponent(applicationComponent: ApplicationComponent) {
        this.applicationComponent = applicationComponent
    }
}