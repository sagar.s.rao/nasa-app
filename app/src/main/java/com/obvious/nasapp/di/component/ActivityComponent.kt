package com.obvious.nasapp.di.component


import com.obvious.nasapp.di.ActivityScope
import com.obvious.nasapp.di.module.ActivityModule
import com.obvious.nasapp.ui.imageDetails.ImageDetailsActivity
import com.obvious.nasapp.ui.imageUi.ImageActivity
import dagger.Component

@ActivityScope
@Component(
    dependencies = [ApplicationComponent::class],
    modules = [ActivityModule::class]
)
interface ActivityComponent {

    fun inject(imageActivity: ImageActivity)
    fun inject(imageDetailsActivity: ImageDetailsActivity)

}