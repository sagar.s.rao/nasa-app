package com.obvious.nasapp.di.component


import com.obvious.nasapp.di.FragmentScope
import com.obvious.nasapp.di.module.FragmentModule
import dagger.Component

@FragmentScope
@Component(
    dependencies = [ApplicationComponent::class],
    modules = [FragmentModule::class]
)
interface FragmentComponent {


}