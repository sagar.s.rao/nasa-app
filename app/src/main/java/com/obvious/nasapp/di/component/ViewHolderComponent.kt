package com.obvious.nasapp.di.component

import com.obvious.nasapp.di.ViewModelScope
import com.obvious.nasapp.di.module.ViewHolderModule
import dagger.Component

@ViewModelScope
@Component(
    dependencies = [ApplicationComponent::class],
    modules = [ViewHolderModule::class]
)
interface ViewHolderComponent {


}