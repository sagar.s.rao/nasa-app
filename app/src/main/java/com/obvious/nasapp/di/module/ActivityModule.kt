package com.obvious.nasapp.di.module

import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.obvious.nasapp.ui.base.BaseActivity
import com.obvious.nasapp.ui.imageUi.ImageActivityViewModel
import com.obvious.nasapp.utils.ViewModelProviderFactory
import com.obvious.nasapp.utils.network.NetworkHelper
import com.obvious.nasapp.utils.rx.SchedulerProvider

import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable

/**
 * Kotlin Generics Reference: https://kotlinlang.org/docs/reference/generics.html
 * Basically it means that we can pass any class that extends BaseActivity which take
 * BaseViewModel subclass as parameter
 */
@Module
class ActivityModule(private val activity: BaseActivity<*>) {

    @Provides
    fun provideLinearLayoutManager(): LinearLayoutManager = LinearLayoutManager(activity)

    @Provides
    fun provideImageRepository(
        schedulerProvider: SchedulerProvider,
        compositeDisposable: CompositeDisposable,
        networkHelper: NetworkHelper
    ): ImageActivityViewModel = ViewModelProviders.of(
        activity, ViewModelProviderFactory(ImageActivityViewModel::class) {
            ImageActivityViewModel(schedulerProvider, compositeDisposable, networkHelper)
        }).get(ImageActivityViewModel::class.java)


}