package com.obvious.nasapp.di.module

import androidx.lifecycle.LifecycleRegistry
import com.obvious.nasapp.di.ViewModelScope
import com.obvious.nasapp.ui.base.BaseItemViewHolder
import dagger.Module
import dagger.Provides

@Module
class ViewHolderModule(private val viewHolder: BaseItemViewHolder<*, *>) {

    @Provides
    @ViewModelScope
    fun provideLifecycleRegistry(): LifecycleRegistry = LifecycleRegistry(viewHolder)
}