package com.obvious.nasapp.ui.imageDetails

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.widget.LinearLayoutCompat
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.lifecycle.Observer
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.obvious.nasapp.R
import com.obvious.nasapp.di.component.ActivityComponent
import com.obvious.nasapp.ui.base.BaseActivity
import com.obvious.nasapp.ui.imageUi.ImageActivity
import com.obvious.nasapp.ui.imageUi.ImageActivityViewModel
import com.obvious.nasapp.ui.imageUi.model.NasaJsonResponseItem
import com.obvious.nasapp.utils.common.Constants
import com.obvious.nasapp.utils.common.OnSwipeTouchListener


class ImageDetailsActivity : BaseActivity<ImageActivityViewModel>() {

    lateinit var mDetailsImage: ImageView
    lateinit var mNasaImgDesc: TextView
    lateinit var mNasaTitle: TextView
    lateinit var mBack: ImageView
    lateinit var mMainLayout: LinearLayoutCompat
    lateinit var mSwipeLeft: ImageView
    lateinit var mSwipeRight: ImageView
    var nasaList: List<NasaJsonResponseItem>? = null
    var count = 0

    override fun provideLayoutId(): Int {
        return R.layout.activity_image_details_new
    }

    override fun injectDependencies(activityComponent: ActivityComponent) {
        activityComponent.inject(this)
    }

    override fun setupView(savedInstanceState: Bundle?) {
        viewModel.getJsonData(this)
        mDetailsImage = findViewById(R.id.iv_nasa_details)
        mNasaImgDesc = findViewById(R.id.tv_nasa_details_desc)
        mNasaTitle = findViewById(R.id.tv_title)
        mBack = findViewById(R.id.iv_back)
        mMainLayout = findViewById(R.id.constraint_image_details)
        mSwipeLeft = findViewById(R.id.iv_swipe_left)
        mSwipeRight = findViewById(R.id.iv_swipe_right)
        mBack.setOnClickListener {
            startActivity(Intent(this, ImageActivity::class.java))
            finish()
        }
        //NASA IMAGE
        if (intent.extras?.getString(Constants.NASA_IMAGE) != null) {
            Glide.with(this)
                .load(intent.extras?.getString(Constants.NASA_IMAGE))
                .apply(
                    RequestOptions().placeholder(R.mipmap.iv_placeholder)
                        .error(R.mipmap.iv_error_placeholder)
                )
                .into(mDetailsImage)
        }
        //NASA TITLE
        if (intent?.extras?.getString(Constants.NASA_IMAGE_TITLE) != null) {
            mNasaTitle.text = intent?.extras?.getString(Constants.NASA_IMAGE_TITLE)
        }
        //NASA IMAGE DESC
        if (intent?.extras?.getString(Constants.NASA_IMAGE_DESC) != null) {
            mNasaImgDesc.text = intent?.extras?.getString(Constants.NASA_IMAGE_DESC)
        }


    }

    @SuppressLint("ClickableViewAccessibility")
    override fun setupObservers() {
        super.setupObservers()

        viewModel.getJsonData.observe(this, Observer { data ->
            val gson = Gson()
            val type = object : TypeToken<List<NasaJsonResponseItem?>?>() {}.type
            nasaList = gson.fromJson(data, type)
            count = intent.extras?.getInt(Constants.NASA_IMAGE_POSITION)!!.toInt()
            count++
            setValues(count)

            mMainLayout.setOnTouchListener(object : OnSwipeTouchListener(this) {

                override fun onSwipeRight() {
                    if (count == nasaList?.size?.minus(1)) {
                        mSwipeRight.visibility = View.GONE
                        finish()
                    } else {
                        mSwipeRight.visibility = View.VISIBLE
                        count++
                        setValues(count)
                    }

                }


                override fun onSwipeLeft() {

                    if (count == nasaList?.size?.minus(1)) {
                        mSwipeLeft.visibility = View.GONE
                        finish()
                    } else {
                        mSwipeLeft.visibility = View.VISIBLE
                        count++
                        setValues(count)
                    }
                }

            })


        })
    }

    private fun setValues(count: Int) {
        Glide.with(this@ImageDetailsActivity)
            .load(nasaList!![count].hdurl)
            .apply(
                RequestOptions().placeholder(R.mipmap.iv_placeholder)
                    .error(R.mipmap.iv_error_placeholder)
            )
            .into(mDetailsImage)
        //NASA TITLE
        mNasaTitle.text = nasaList!![count].title.toString()
        //NASA IMAGE DESC
        mNasaImgDesc.text = nasaList!![count].explanation.toString()
    }
}