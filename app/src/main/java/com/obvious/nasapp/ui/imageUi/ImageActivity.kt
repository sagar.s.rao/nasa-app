package com.obvious.nasapp.ui.imageUi

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ProgressBar
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.google.gson.Gson
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import com.google.gson.reflect.TypeToken
import com.obvious.nasapp.R
import com.obvious.nasapp.di.component.ActivityComponent
import com.obvious.nasapp.ui.base.BaseActivity
import com.obvious.nasapp.ui.imageUi.adapter.NasaAdapter
import com.obvious.nasapp.ui.imageUi.model.NasaJsonResponseItem
import com.obvious.nasapp.utils.display.ScreenUtils
import com.obvious.nasapp.utils.network.NetworkHelper
import org.json.JSONArray
import java.io.File
import java.io.FileInputStream

class ImageActivity : BaseActivity<ImageActivityViewModel>() {

    lateinit var mProgressBar: ProgressBar
    lateinit var mRvImages: RecyclerView

    override fun provideLayoutId(): Int {
        return R.layout.activity_main
    }

    override fun injectDependencies(activityComponent: ActivityComponent) {
        activityComponent.inject(this)
    }

    override fun setupView(savedInstanceState: Bundle?) {
        initialiseUI()
        if(NetworkHelper(this).isNetworkConnected()){
            viewModel.getJsonData(this)
        }else{
            showMessage(getString(R.string.network_connection_error))
        }

    }


    private fun initialiseUI() {
        mProgressBar = findViewById(R.id.pb_images)
        mRvImages = findViewById(R.id.rv_images)
    }

    override fun setupObservers() {
        super.setupObservers()
        viewModel.getJsonData.observe(this, { data ->
            val gson = Gson()
            val type = object : TypeToken<List<NasaJsonResponseItem?>?>() {}.type
            val nasaList: List<NasaJsonResponseItem> = gson.fromJson(data, type)
            if (nasaList.isNotEmpty()){
                mProgressBar.visibility = View.GONE
                mRvImages.apply {
                    layoutManager = StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL)
                    adapter = NasaAdapter(nasaList.sortedByDescending { it.date }, this@ImageActivity)

                }
            }



        })
    }
}