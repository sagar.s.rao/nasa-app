package com.obvious.nasapp.ui.imageUi

import android.content.Context
import androidx.lifecycle.MutableLiveData
import com.obvious.nasapp.NASAApplication
import com.obvious.nasapp.ui.base.BaseViewModel
import com.obvious.nasapp.utils.display.ScreenUtils
import com.obvious.nasapp.utils.network.NetworkHelper
import com.obvious.nasapp.utils.rx.SchedulerProvider
import io.reactivex.disposables.CompositeDisposable

class ImageActivityViewModel(
    schedulerProvider: SchedulerProvider,
    compositeDisposable: CompositeDisposable,
    networkHelper: NetworkHelper

) : BaseViewModel(schedulerProvider, compositeDisposable, networkHelper) {

    val getJsonData = MutableLiveData<String>()
    override fun onCreate() {

    }

    fun getJsonData(context: Context) {
        getJsonData.postValue(ScreenUtils.getJsonData(context, "data.json"))
    }

}