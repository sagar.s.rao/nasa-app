package com.obvious.nasapp.ui.imageUi.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.obvious.nasapp.R
import com.obvious.nasapp.ui.imageUi.model.NasaJsonResponseItem

import com.bumptech.glide.request.RequestOptions

import com.bumptech.glide.Glide
import com.obvious.nasapp.ui.imageDetails.ImageDetailsActivity
import com.obvious.nasapp.utils.common.Constants


class NasaAdapter(
    private val mNasaList: List<NasaJsonResponseItem>,
    private val mContext: Context
) : RecyclerView.Adapter<NasaAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NasaAdapter.ViewHolder {

        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.adapter_nasa, parent, false)

        return ViewHolder(view)
    }

    class ViewHolder(ItemView: View) : RecyclerView.ViewHolder(ItemView) {
        val ivNasa: ImageView = itemView.findViewById(R.id.iv_nasa)
    }

    override fun onBindViewHolder(holder: NasaAdapter.ViewHolder, position: Int) {

        val mImageListData = mNasaList[position]
        Glide.with(mContext)
            .load(mImageListData.hdurl)
            .apply(
                RequestOptions().placeholder(R.mipmap.iv_placeholder)
                    .error(R.mipmap.iv_error_placeholder)
            )
            .into(holder.ivNasa)

        holder.ivNasa.setOnClickListener {

            val intent = Intent(mContext,ImageDetailsActivity::class.java)
            intent.putExtra(Constants.NASA_IMAGE,mImageListData.hdurl)
            intent.putExtra(Constants.NASA_IMAGE_DESC,mImageListData.explanation)
            intent.putExtra(Constants.NASA_IMAGE_TITLE,mImageListData.title)
            intent.putExtra(Constants.NASA_IMAGE_POSITION,position)
            mContext.startActivity(intent)


        }
    }

    override fun getItemCount(): Int {
        return if (mNasaList.isEmpty())
            0
        else
            mNasaList.size

    }


}