package com.obvious.nasapp.utils.common

object Constants {
    const val NASA_IMAGE = "NASA_IMAGE"
    const val NASA_IMAGE_DESC = "NASA_IMAGE_DESC"
    const val NASA_IMAGE_TITLE = "NASA_IMAGE_TITLE"
    const val NASA_IMAGE_POSITION = "NASA_IMAGE_POSITION"
}