package com.obvious.nasapp.utils.common

enum class Status {
    SUCCESS,
    ERROR,
    LOADING,
    UNKNOWN
}