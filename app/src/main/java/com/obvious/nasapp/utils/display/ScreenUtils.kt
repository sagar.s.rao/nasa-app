package com.obvious.nasapp.utils.display

import android.content.Context
import android.content.res.Resources
import java.io.IOException

object ScreenUtils {



    fun getJsonData(context: Context, fileName: String): String? {
        val jsonString: String
        try {
            jsonString = context.assets.open(fileName).bufferedReader().use { it.readText() }
        } catch (ioException: IOException) {
            ioException.printStackTrace()
            return null
        }
        return jsonString
    }
}